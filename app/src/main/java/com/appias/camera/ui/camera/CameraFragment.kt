package com.appias.camera.ui.camera

import android.app.Activity.RESULT_OK
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.appias.camera.R
import com.appias.camera.model.Camera
import com.appias.camera.ui.filter.FilterFragment
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import io.fotoapparat.Fotoapparat
import io.fotoapparat.log.logcat
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.result.transformer.scaled
import kotlinx.android.synthetic.main.fr_camera.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import android.content.Intent
import android.view.*
import com.appias.camera.extension.addFragment
import timber.log.Timber


class CameraFragment : MvpAppCompatFragment(), CameraView {

    private var activeCamera = Camera.Back
    private var fotoapparat: Fotoapparat? = null
    private var isActiveCameraBack = true

    @InjectPresenter
    lateinit var cameraPresenter: CameraPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fr_camera, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivTurnCamera.setOnClickListener { returnCamera() }
        ivSnap.setOnClickListener { makePhoto() }
        ivGallery.setOnClickListener { loadFromGallery() }
        fotoapparat = Fotoapparat.with(context!!)
            .into(cameraView)
            .focusView(focusView)
            .previewScaleType(ScaleType.CenterCrop)
            .lensPosition(activeCamera.lensPosition)
            .logger(logcat())
            .build()
    }

    override fun onResume() {
        super.onResume()
        fotoapparat?.start()
    }

    override fun onStop() {
        super.onStop()
        fotoapparat?.stop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == SELECT_PHOTO && data != null && data.data != null) {
            startFilterFragment(FilterFragment.getFragment(data.data!!))
        }
    }

    override fun photoSaved(absolutePath: String) {
        startFilterFragment(FilterFragment.getFragment(absolutePath))
    }

    private fun startFilterFragment(fragment: Fragment) {
        fragmentManager?.addFragment(fragment)?.apply {
            addToBackStack = true
            commit()
        }
        idProgress.visibility = View.GONE
    }

    private fun makePhoto() {
        idProgress.visibility = View.VISIBLE
        val photoResult = fotoapparat!!.takePicture()
        val date = Date(System.currentTimeMillis())
        val simpleFormat = SimpleDateFormat(TIME_FORMAT, Locale.US)
        val nameFile = getString(R.string.photo_name, simpleFormat.format(date))
        val file = File(activity!!.getExternalFilesDir(TYPE_DIR), nameFile)
        photoResult.saveToFile(file)
        photoResult
            .toBitmap(scaled(SCALE_FACTOR))
            .transform { bitmapPhoto -> cameraPresenter.savePhoto(bitmapPhoto, file.absolutePath) }
    }


    private fun loadFromGallery() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = TYPE_INTENT
        startActivityForResult(photoPickerIntent, SELECT_PHOTO)
    }

    private fun returnCamera() {
        isActiveCameraBack = !isActiveCameraBack
        activeCamera = if (activeCamera === Camera.Back) Camera.Front else Camera.Back
        fotoapparat!!.switchTo(
            activeCamera.lensPosition,
            activeCamera.configuration
        )
        val idDrawable =
            if (isActiveCameraBack) R.drawable.ic_camera_front else R.drawable.ic_camera_rear
        ivTurnCamera.setImageResource(idDrawable)
    }

    companion object {
        private const val SCALE_FACTOR = 0.25f
        private const val TIME_FORMAT = "HH_mm_ss"
        private const val TYPE_DIR = "photos"
        private const val SELECT_PHOTO = 101
        private const val TYPE_INTENT = "image/*"

        fun getInstance(): Fragment = CameraFragment()
    }
}