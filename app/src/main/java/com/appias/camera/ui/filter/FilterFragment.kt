package com.appias.camera.ui.filter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.appias.camera.R
import com.arellomobile.mvp.MvpAppCompatFragment
import android.net.Uri
import android.provider.MediaStore
import android.widget.SeekBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import com.appias.camera.ui.filter.adapter.FilterAdapter
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fr_filter.*
import org.koin.android.ext.android.get
import android.content.Intent
import android.graphics.Bitmap.CompressFormat
import android.graphics.drawable.BitmapDrawable
import android.os.Environment
import androidx.core.graphics.drawable.toBitmap
import java.io.File
import java.io.FileOutputStream
import android.os.StrictMode
import androidx.core.content.FileProvider
import timber.log.Timber
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Locale.*
import kotlin.collections.ArrayList


class FilterFragment : MvpAppCompatFragment(), FilterView {

    private val filterAdapter = FilterAdapter()
    @InjectPresenter
    lateinit var filterPresenter: FilterPresenter

    private var filePath: String = ""

    @ProvidePresenter
    fun provide(): FilterPresenter {
        return get()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fr_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let { bundle ->
            bundle.getString(ABSOLUTE_PATH_IMAGE)?.let {
                filePath = it
                filterPresenter.setSourceBitmap(BitmapFactory.decodeFile(it))
            }
            bundle.getParcelable<Uri>(URI_PATH)?.let {
                filePath = it.path!!
                filterPresenter.setSourceBitmap(
                    MediaStore.Images.Media.getBitmap(context!!.contentResolver, it)
                )
            }
        }
        rvFilters.apply {
            layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
            adapter = filterAdapter
        }
        sbWarm.setOnSeekBarChangeListener(seekBarListener)
        sbContrast.setOnSeekBarChangeListener(seekBarListener)
        sbSaturation.setOnSeekBarChangeListener(seekBarListener)
        sbProgress.setOnSeekBarChangeListener(seekBarListener)
        ivClose.setOnClickListener { showEasyFilter(false) }
        filterAdapter.setOnclickListener {
            filterPresenter.setFilter(it)
            sbProgress.progress = 100
        }
        toolbar.setOnMenuItemClickListener {
            shareImage()
            true
        }
    }

    override fun setFilters(filters: ArrayList<String>) {
        filterAdapter.setData(filters)
    }

    override fun setBitmap(bitmap: Bitmap?) {
        ivImage.apply {
            setImageBitmap(bitmap)
            invalidate()
        }
        sbProgress.apply {
            progress = 100
            visibility = View.GONE
        }
    }

    override fun showEasyFilter(isVisible: Boolean) {
        sbProgress.apply {
            progress = 100
            visibility = View.GONE
        }
        groupSeekBar.visibility = if (isVisible) View.VISIBLE else View.GONE
        rvFilters.visibility = if (!isVisible) View.VISIBLE else View.GONE
    }

    override fun setFilterBitmap(bitmap: Bitmap?) {
        sbProgress.visibility = View.VISIBLE
        ivImage.apply {
            setImageBitmap(bitmap)
            invalidate()
        }
    }

    override fun dropEasyFilter() {
        ivImage.apply {
            warmth = 1f
            contrast = 1f
            saturation = 1f
        }
    }

    private val seekBarListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
        }

        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            when (seekBar.id) {
                R.id.sbWarm -> ivImage.warmth = progress / 100f
                R.id.sbContrast -> ivImage.contrast = progress / 100f
                R.id.sbSaturation -> ivImage.saturation = progress / 100f
                R.id.sbProgress -> filterPresenter.setRenderFilter(value = progress)
            }
        }
    }

    private fun shareImage() {
        val bitmap = ivImage.drawable.toBitmap()
        saveImage(bitmap)?.let {
            shareImageUri(it)
        }
    }

    private fun saveImage(image: Bitmap): Uri? {
        val imagesFolder = File(context!!.cacheDir, "images")
        var uri: Uri? = null
        try {
            imagesFolder.mkdirs()
            val file = File(imagesFolder, "shared_image.jpg")

            val stream = FileOutputStream(file)
            image.compress(CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
            uri = FileProvider.getUriForFile(context!!, APPLICATION_ID, file)

        } catch (e: IOException) {
            Timber.e("IOException while trying to write file for sharing:  ${e.message}")
        }
        return uri
    }

    private fun shareImageUri(uri: Uri) {
        val intent = Intent(android.content.Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.type = "image/png"
        startActivity(intent)
    }

    companion object {
        private const val ABSOLUTE_PATH_IMAGE = "ABSOLUTE_PATH_IMAGE"
        private const val URI_PATH = "URI_PATH"
        private const val APPLICATION_ID = "com.appias.camera.fileprovider"
        private const val REQUEST_CAPTURE_IMAGE = 11

        fun getFragment(absolutePath: String): Fragment {
            val fragment = FilterFragment()
            val bundle = Bundle()
            bundle.putString(ABSOLUTE_PATH_IMAGE, absolutePath)
            fragment.arguments = bundle
            return fragment
        }

        fun getFragment(uri: Uri): Fragment {
            val fragment = FilterFragment()
            val bundle = Bundle()
            bundle.putParcelable(URI_PATH, uri)
            fragment.arguments = bundle
            return fragment
        }
    }
}