package com.appias.camera.ui.filter

import android.graphics.Bitmap
import com.arellomobile.mvp.MvpView

interface FilterView : MvpView {

    fun setBitmap(bitmap: Bitmap?)

    fun setFilterBitmap(bitmap: Bitmap?)

    fun setFilters(filters: ArrayList<String>)

    fun showEasyFilter(isVisible: Boolean)

    fun dropEasyFilter()
}