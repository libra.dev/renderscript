package com.appias.camera.ui.filter.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appias.camera.R
import com.appias.camera.model.Filter
import kotlinx.android.synthetic.main.item_filter.view.*

class FilterAdapter : RecyclerView.Adapter<FilterAdapter.FilterViewHolder>() {
    private var filterList = arrayListOf<String>()
    private var onItemClick: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_filter, parent, false)
        return FilterViewHolder(view, onItemClick)
    }

    override fun getItemCount(): Int = filterList.size

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
        holder.bind(filterList[position])
    }

    fun setData(data: ArrayList<String>) {
        filterList = data
        notifyDataSetChanged()
    }

    fun setOnclickListener(callback: ((String) -> Unit)) {
        onItemClick = callback
    }

    inner class FilterViewHolder(
        view: View,
        onItemClick: ((String) -> Unit)?
    ) : RecyclerView.ViewHolder(view) {
        fun bind(@Filter filter: String) {
            itemView.tvTitle.text = filter
            itemView.setOnClickListener { onItemClick?.invoke(filter) }
        }
    }
}