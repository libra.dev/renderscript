package com.appias.camera.ui.camera

import android.graphics.Bitmap
import com.appias.camera.extension.rotateImage
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.fotoapparat.result.BitmapPhoto
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.io.FileOutputStream
import java.io.IOException


@InjectViewState
class CameraPresenter : MvpPresenter<CameraView>() {

    private var disposable: Disposable? = null

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }

    fun savePhoto(bitmapPhoto: BitmapPhoto, absolutePath: String) {
        disposable = Observable.just(bitmapPhoto)
            .map { (bitmap, rotationDegrees) ->
                bitmap.rotateImage(bitmap, -rotationDegrees)
            }
            .map { bitmap -> saveFile(bitmap, absolutePath) }
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ viewState.photoSaved(absolutePath) }, Timber::e)
    }

    private fun saveFile(bitmap: Bitmap, absolutePath: String): Bitmap {
        try {
            FileOutputStream(absolutePath).use { out ->
                bitmap.compress(
                    Bitmap.CompressFormat.PNG,
                    100,
                    out
                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bitmap
    }
}