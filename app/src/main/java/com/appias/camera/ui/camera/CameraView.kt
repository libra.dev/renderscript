package com.appias.camera.ui.camera

import com.arellomobile.mvp.MvpView

interface CameraView : MvpView {
    fun photoSaved(absolutePath: String)
}