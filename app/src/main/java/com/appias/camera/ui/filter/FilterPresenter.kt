package com.appias.camera.ui.filter

import android.graphics.Bitmap
import com.appias.camera.model.Filter
import com.appias.camera.repository.FilterRepository
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit


@InjectViewState
class FilterPresenter(private val filterRepository: FilterRepository) : MvpPresenter<FilterView>() {
    private val list = arrayListOf(
        Filter.STANDARD,
        Filter.IMAGE_FILTER,
        Filter.MONO,
        Filter.BRIDGE,
        Filter.BLUR,
        Filter.HISTOGRAM,
        Filter.CONVOLVE
    )
    private var sourceBitmap: Bitmap? = null
    private var currentProgress = 100
    private var currentFilter = Filter.STANDARD
    private var disposable: Disposable? = null


    init {
        viewState.setFilters(list)
    }

    override fun onDestroy() {
        unsubscribe()
        filterRepository.clear()
        super.onDestroy()
    }

    fun setSourceBitmap(bitmap: Bitmap) {
        sourceBitmap = bitmap
        viewState.setBitmap(bitmap)
        filterRepository.initAllocation(bitmap)
    }


    fun setFilter(@Filter filter: String) {
        currentFilter = filter
        when (filter) {
            Filter.IMAGE_FILTER -> viewState.showEasyFilter(true)
            Filter.STANDARD -> {
                viewState.setBitmap(sourceBitmap)
                viewState.dropEasyFilter()
            }
            else -> setRenderFilter(currentProgress)
        }
    }

    fun setRenderFilter(value: Int) {
        if (currentFilter != Filter.STANDARD && currentFilter != Filter.IMAGE_FILTER) {
            unsubscribe()
            disposable = Observable.just(filterRepository.filter(currentFilter, value))
                .debounce(150, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.setFilterBitmap(it)
                }, Timber::e)
        }

    }

    fun shareImage() {

    }

    private fun unsubscribe() {
        disposable?.let {
            it.dispose()
        }
        disposable = null
    }
}