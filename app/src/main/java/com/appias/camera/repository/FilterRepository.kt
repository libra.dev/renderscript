package com.appias.camera.repository

import android.content.Context
import android.graphics.Bitmap
import android.renderscript.*
import com.appias.camera.model.Filter
import ru.appias.renderscriptexp.ScriptC_Bridge
import ru.appias.renderscriptmono.ScriptC_Mono
import java.util.*
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin

class FilterRepository(context: Context) {

    private val renderScript = RenderScript.create(context)
    private var bitmapOutput: Bitmap? = null
    private var inAllocation: Allocation? = null
    private var outAllocation: Allocation? = null

    fun initAllocation(sourceBitmap: Bitmap) {
        bitmapOutput =
            Bitmap.createBitmap(sourceBitmap.width, sourceBitmap.height, sourceBitmap.config)
        inAllocation = Allocation.createFromBitmap(
            renderScript, sourceBitmap,
            Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT
        )
        outAllocation = Allocation.createTyped(renderScript, inAllocation!!.type)
    }

    fun clear() {
        inAllocation?.let {
            it.destroy()
            inAllocation = null
        }
        outAllocation?.let {
            it.destroy()
            outAllocation = null
        }
    }

    fun filter(@Filter filter: String, progress: Int): Bitmap {
        return when (filter) {
            Filter.BLUR -> blur(progress)
            Filter.HISTOGRAM -> matrix(progress)
            Filter.MONO -> mono(progress)
            Filter.CONVOLVE -> convolve(progress)
            Filter.BRIDGE -> bridge(progress)
            else -> throw (UnknownFormatFlagsException("Error unknown type filter"))
        }
    }


    private fun blur(progress: Int): Bitmap {
        val value = ((25 - 1) * (progress / 100.0f) + 1)
        val script = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript))
        script.setInput(inAllocation)
        script.setRadius(value)
        script.forEach(outAllocation)
        outAllocation!!.copyTo(bitmapOutput)
        return bitmapOutput!!
    }


    private fun matrix(progress: Int): Bitmap {
        val value = progress / 10f
        val cos = cos(value)
        val sin = sin(value)
        val mat = Matrix3f()
        mat.set(0, 0, (.299 + .701 * cos + .168 * sin).toFloat())
        mat.set(1, 0, (.587 - .587 * cos + .330 * sin).toFloat())
        mat.set(2, 0, (.114 - .114 * cos - .497 * sin).toFloat())
        mat.set(0, 1, (.299 - .299 * cos - .328 * sin).toFloat())
        mat.set(1, 1, (.587 + .413 * cos + .035 * sin).toFloat())
        mat.set(2, 1, (.114 - .114 * cos + .292 * sin).toFloat())
        mat.set(0, 2, (.299 - .3 * cos + 1.25 * sin).toFloat())
        mat.set(1, 2, (.587 - .588 * cos - 1.05 * sin).toFloat())
        mat.set(2, 2, (.114 + .886 * cos - .203 * sin).toFloat())

        val script = ScriptIntrinsicColorMatrix.create(renderScript, Element.U8_4(renderScript))
        script.setColorMatrix(mat)
        script.forEach(inAllocation, outAllocation)
        outAllocation?.copyTo(bitmapOutput)
        return bitmapOutput!!
    }

    private fun mono(
        progress: Int
    ): Bitmap {
        val script = ScriptC_Mono(renderScript)
        script._valueProgress = abs(progress / 100f - 1)
        script.forEach_root(inAllocation, outAllocation)
        outAllocation?.copyTo(bitmapOutput)
        return bitmapOutput!!
    }

    private fun convolve(
        progress: Int
    ): Bitmap {
        val f1 = progress.toFloat()
        val f2 = 1.0f - f1

        // Emboss filter kernel
        val coefficients = floatArrayOf(
            -f1 * 2f,
            0f,
            -f1,
            0f,
            0f,
            0f,
            -f2 * 2,
            -f2,
            0f,
            0f,
            -f1,
            -f2,
            1f,
            f2,
            f1,
            0f,
            0f,
            f2,
            f2 * 2,
            0f,
            0f,
            0f,
            f1,
            0f,
            f1 * 2
        )
        val script = ScriptIntrinsicConvolve5x5.create(renderScript, Element.U8_4(renderScript))
        script.setInput(inAllocation)
        script.setCoefficients(coefficients)
        script.forEach(outAllocation)
        outAllocation?.copyTo(bitmapOutput)
        return bitmapOutput!!
    }

    private fun bridge(
        progress: Int
    ): Bitmap {
        val script = ScriptC_Bridge(renderScript)
        script.invoke_setBridge(progress.toFloat())
        script.forEach_root(inAllocation, outAllocation)
        outAllocation?.copyTo(bitmapOutput)
        return bitmapOutput!!
    }
}