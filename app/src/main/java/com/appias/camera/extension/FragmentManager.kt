package com.appias.camera.extension

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.appias.camera.R


fun FragmentManager.addFragment(fragment: Fragment, containerId: Int = R.id.frContainer): Transaction {
    return Transaction(this, fragment, Transaction.Type.ADD, containerId)
}

fun FragmentManager.replaceFragment(fragment: Fragment, containerId: Int = R.id.frContainer): Transaction {
    return Transaction(this, fragment, Transaction.Type.REPLACE, containerId)
}