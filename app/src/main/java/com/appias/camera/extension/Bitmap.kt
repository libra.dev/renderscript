package com.appias.camera.extension

import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import java.io.IOException

fun Bitmap.cropCenter(srcBmp: Bitmap): Bitmap {
    return if (srcBmp.width >= srcBmp.height) {
        val center = srcBmp.width / 2 - srcBmp.height / 2
        Bitmap.createBitmap(srcBmp, center, 0, srcBmp.height, srcBmp.height)
    } else {
        val center = srcBmp.height / 2 - srcBmp.width / 2
        Bitmap.createBitmap(srcBmp, 0, center, srcBmp.width, srcBmp.width)
    }
}

fun Bitmap.rotateImage(source: Bitmap, angle: Int): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(angle.toFloat())
    return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
}

fun Bitmap.checkImage(image: Bitmap, photoPath: String): Bitmap? {
    var bitmap = image
    var ei: ExifInterface? = null
    var rotatedBitmap: Bitmap? = null
    if (bitmap.width > 1024 && bitmap.height > 1024)
        bitmap = Bitmap.createScaledBitmap(cropCenter(bitmap)!!, 1024, 1024, true)
    try {
        ei = ExifInterface(photoPath)

        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        rotatedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> bitmap
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return rotatedBitmap
}

