package com.appias.camera.di

import com.appias.camera.repository.FilterRepository
import com.appias.camera.ui.filter.FilterPresenter
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {
    factory { FilterRepository(context = androidContext()) }

    factory { FilterPresenter(get()) }
}