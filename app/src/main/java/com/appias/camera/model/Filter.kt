package com.appias.camera.model

import androidx.annotation.StringDef
import com.appias.camera.model.Filter.Companion.BLUR
import com.appias.camera.model.Filter.Companion.MONO
import com.appias.camera.model.Filter.Companion.STANDARD

@StringDef(MONO, BLUR, STANDARD)
annotation class Filter {
    companion object {
        const val MONO = "mono"
        const val BLUR = "blur"
        const val HISTOGRAM="histogram"
        const val CONVOLVE="convolve"
        const val BRIDGE="bridge"

        const val IMAGE_FILTER = "easy filter"
        const val STANDARD = "standard"
    }
}