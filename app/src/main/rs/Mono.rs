#pragma version(1)
#pragma rs java_package_name(ru.appias.renderscriptmono)

const static float3 gMonoMult = {0.299f, 0.587f, 0.114f};

float valueProgress = 0.f;

void root(const uchar4 *v_in, uchar4 *v_out) {
  //unpack a color to a float4
  float4 f4 = rsUnpackColor8888(*v_in);
  //take the dot product of the color and the multiplier
  float3 mono = dot(f4.rgb, gMonoMult);

  float3 result = mix(mono, f4.rgb, valueProgress);

   *v_out = rsPackColorTo8888(result);
}