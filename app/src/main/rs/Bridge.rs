#pragma version(1)
#pragma rs java_package_name(ru.appias.renderscriptexp)

static float bridge = 0.f;

void setBridge(float v){
 bridge=255.f/(255.f-v);
}

void root(const uchar4 *v_in, uchar4 *v_out) {
   float4 f4 = rsUnpackColor8888(*v_in);
   *v_out=rsPackColorTo8888(f4.rgb);
   v_out->r=clamp((int) (bridge*v_in->r),0,255);
   v_out->g=clamp((int) (bridge*v_in->g),0,255);
   v_out->b=clamp((int) (bridge*v_in->b),0,255);
}