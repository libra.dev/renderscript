package com.appias.camera

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}

/**
    _                _                    _        __                          _
    | |_   ___   ___ | |_     _ _    ___  | |_     / _|  ___   _  _   _ _    __| |
    |  _| / -_) (_-< |  _|   | ' \  / _ \ |  _|   |  _| / _ \ | || | | ' \  / _` |
     \__| \___| /__/  \__|   |_||_| \___/  \__|   |_|   \___/  \_,_| |_||_| \__,_|
                                                                                   **/
